import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_lib/state/custom_state.dart';
import '../themes/brightness_cubit.dart';
import '../state/counter_bloc.dart';

/// {@template counter_view}
/// A [StatelessWidget] that:
/// * demonstrates how to consume and interact with a [CounterBloc].
/// {@endtemplate}
class CounterView extends StatelessWidget {
  /// {@macro counter_view}
  const CounterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Counter')),
      body: Center(
        // child: BlocBuilder<CounterBloc, int>(
        //   buildWhen: (previousState, count) {
        //     // return true/false to determine whether or not
        //     // to rebuild the widget with state
        //     return count == 2; //only rebuilds if condition is true
        //   },
        //   builder: (context, count) {
        //     return Column(
        //       children: [
        //         TextButton(
        //           onPressed: () {},
        //           child: const Text("click here!"),
        //         ),
        //         const SizedBox(
        //           height: 16,
        //         ),
        //         Text('$count', style: Theme.of(context).textTheme.headline1),
        //       ],
        //     );
        //   },
        // ),
        child: BlocBuilder<CustomBloc, String?>(
          builder: (context, msg) {
            return BlocBuilder<CounterBloc, int>(
              buildWhen: (previousState, count) {
                // return true/false to determine whether or not
                // to rebuild the widget with state
                //return count == 2; //only rebuilds if condition is true
                return true; //redundant
              },
              builder: (context, count) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        context.read<CustomBloc>().add(ToggleText());
                      },
                      child: Text(
                          "(MultiBlocProvider) ${msg == null ? 'click to show' : 'click to hide'}"),
                    ),
                    if (msg != null) Text(msg),
                    const SizedBox(
                      height: 16,
                    ),
                    Text('$count',
                        style: Theme.of(context).textTheme.headline1),
                  ],
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () => context.read<CounterBloc>().add(Increment()),
          ),
          const SizedBox(height: 4),
          FloatingActionButton(
            child: const Icon(Icons.remove),
            onPressed: () => context.read<CounterBloc>().add(Decrement()),
          ),
          const SizedBox(height: 4),
          FloatingActionButton(
            child: const Icon(Icons.brightness_6),
            onPressed: () => context.read<ThemeCubit>().toggleTheme(),
          ),
        ],
      ),
    );
  }
}
