import 'package:flutter_bloc/flutter_bloc.dart';

/// Event being processed by [CustomBloc].
abstract class CustomEvent {}

/// Notifies bloc to increment state.
class ToggleText extends CustomEvent {}

/// Notifies bloc to decrement state.
//class Decrement extends CustomEvent {}

/// {@template custom_bloc}
/// A simple [Bloc] that manages an `string` as its state.
/// {@endtemplate}
class CustomBloc extends Bloc<CustomEvent, String?> {
  /// {@macro custom_bloc}
  CustomBloc() : super(null) {
    on<ToggleText>((event, emit) => emit(state == null ? "clicked!" : null));
  }
}
