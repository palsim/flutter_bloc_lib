import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_lib/state/custom_state.dart';
import '../state/counter_bloc.dart';
import '../views/counter_view.dart';

/// {@template counter_page}
/// A [StatelessWidget] that:
/// * provides a [CounterBloc] to the [CounterView].
/// {@endtemplate}
class CounterPage extends StatelessWidget {
  /// {@macro counter_page}
  const CounterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //Single BlockProvider
    // return BlocProvider(
    //   create: (_) => CounterBloc(),
    //   child: const CounterView(),
    // );

    //MultiBlocProvider
    return MultiBlocProvider(
      providers: [
        BlocProvider<CounterBloc>(
          create: (_) => CounterBloc(),
        ),
        BlocProvider<CustomBloc>(
          create: (_) => CustomBloc(),
        ),
      ],
      child: const CounterView(),
    );
  }
}
